pub struct OpIndex {
    pub outind: u8,
    pub inind: u8,
    pub pipe: u8,
    pub cmd: Vec<String>,
    pub in_redir: String,   //indir path
    pub out_redir: String,  //out_redir path
}

impl OpIndex {
   pub fn new(cmd: Vec<String>) -> OpIndex {
        OpIndex {outind: 20, inind: 20,pipe: 20,
        cmd, in_redir: String::from(""),  out_redir: String::from("")
        }
    }
   pub fn set_path(&mut self) -> bool {
       let input = self.set_inredir_path();
       let output = self.set_outredir_path();
       input.is_ok() && output.is_ok() && self.is_pipeop_last()
   }
   fn is_pipeop_last(&self) -> bool {
     usize::from(self.pipe) != (self.cmd.len() - 1)
   }
   fn set_inredir_path(&mut self) -> Result<(),()> {
       if self.inind < 20 {
           let file_path = self.cmd.get(usize::from(self.inind) + 1);
           match file_path {
               Some(file) => {self.in_redir = file.to_string();
                   return Ok(())
               }
               None => return Err(())
           };
        }
       Ok(())
   }
   fn set_outredir_path(&mut self) -> Result<(),()> {
       if self.outind < 20 {
           let file_path = self.cmd.get(usize::from(self.outind) + 1);
           match file_path {
               Some(file) => {self.out_redir = file.to_string();
                   return Ok(())
               }
               None => return Err(())
           };
        }
       Ok(())
   }

  
   pub fn set_index(&mut self)   {
       let mut index = 0;
       for x in self.cmd.iter(){
           match x.clone().as_str() {
               "<" => self.inind = index,
               ">" => self.outind = index,
               "|" => self.pipe = index,
               _ => ()
           };
           index += 1;
       }
   } 
}


