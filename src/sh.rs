use crate::parse;
use std::{io::{self, Read, Write}, process::{Command, Stdio}};
use std::fs::File;

fn read_file(file: &String) -> Result<String,io::Error> {
    let mut file = File::open(file)?;
    let mut file_data = String::new();
    file.read_to_string(&mut file_data)?;
    Ok(file_data)
}

fn write_file(file: &str,data: &String) ->Result<(),io::Error> {

    let mut file = File::create(file)?;
    file.write_all(&data.trim().as_bytes())?;
    file.write_all("\n".as_bytes())?;
    Ok(())
}
//This function writes the "content" arg, if available, or content the of "file" arg to stdin of the
//child process and returns the prints the output or writes the output of > operator is provided.
//example command wc -c < input_file > output_file or dmesg | grep 9
fn read_stdin(cmd: &Vec<String>,content: Option<&String>, file: &String,out_index: u8, out_file: &String) -> Result<String,io::Error> {
    let mut child = Command::new(&cmd[0])
        .args(&cmd[1..])
        .stdin(Stdio::piped())
        .stdout(Stdio::piped())
        .spawn()?;
    //println!("{} and data {}",&cmd[0],content.unwrap());
    let data = if content.is_some() {
        content.unwrap().to_string()
    } else {
        read_file(file)?
    };
    let stdin = child.stdin.as_mut().unwrap();
    stdin.write_all(&data.as_bytes())?;         //writes data to child stdin
    let ch_out = child.wait_with_output()?;
    let out = String::from_utf8_lossy(&ch_out.stdout).to_string();
    if out_index < 20 {                         //output?
        match write_file(out_file,&out) {
            Ok(_) => Ok("".to_string()),
            Err(e) => Err(e)
        }
    }
    else {
        Ok(out)
    }    
}
//This function returns the output of the the command or io error because of incorrect command or
//argument
//works for command without operators and for output redir. ex: wc -c file and wc file > outpufile
fn execute(cmd: &Vec<String>,file: &String,index: u8,pipe: u8) -> Result<String,io::Error> {
    let mut output = Command::new(&cmd[0]);
    output.args(&cmd[1..]);
    if index < 20 || pipe < 20 {
        output.stdout(Stdio::piped());
    }
    let ch_out = output.spawn()?.wait_with_output()?;                        //execute the child process  //wait until the process is fininshed
    let out = String::from_utf8_lossy(&ch_out.stdout).to_string(); 

    if index < 20 {  //if > redirect is provided, write the output to a specified file
         match write_file(file,&out) {
            Ok(_) => Ok("".to_string()),
            Err(e) => Err(e)
        }
    }
    else {
        Ok(out)
    }  
}
//cat < Cargo.toml | grep 2018
fn pipe_exec(index: &parse::OpIndex) ->Result<String,io::Error> {
    let mut data: String;
    let mut c: Vec<String>;
    //process 1
    if index.inind < 20 {   //read from file
        c = index.cmd[0..usize::from(index.inind)].to_vec();
        data = read_stdin(&c, None, &index.in_redir , 20, &index.out_redir)?;
    } else {    
        c = index.cmd[0..usize::from(index.pipe)].to_vec();
        data = execute(&c, &index.out_redir, 20,index.pipe)?;
    }//end process 1

    if index.outind < 20 {
        c = index.cmd[usize::from(index.pipe + 1)..usize::from(index.outind)].to_vec();
        read_stdin(&c, Some(&data), &index.in_redir, index.outind, &index.out_redir)?;
        return Ok("".to_string());
    }
    
    else {
        c = index.cmd[usize::from(index.pipe + 1)..].to_vec();
        data = read_stdin(&c, Some(&data), &index.in_redir, 20, &index.out_redir)?;
    }

    Ok(data)
}
fn check_results(result: Result<String,io::Error>) {
    match result {
           Ok(res) =>  print!("{}",res),
           Err(e) => println!("{}",e)
       }
}

pub fn exec_cmd(index: &parse::OpIndex) {
   if index.pipe < 20 {
       //println!("test");
       let res = pipe_exec(&index);
       check_results(res);
   } 
   else if index.inind < 20 {
       let cmd_upto_redir = &index.cmd[0..usize::from(index.inind)].to_vec();
       let res =  read_stdin(cmd_upto_redir, None, &index.in_redir, index.outind, &index.out_redir);
       check_results(res);
    }
   else if index.outind < 20 {
        let cmd_upto_redir = &index.cmd[0..usize::from(index.outind)].to_vec();
        let res = execute(cmd_upto_redir, &index.out_redir, index.outind,20);
        check_results(res);
   }
   else {
       let res = execute(&index.cmd, &index.out_redir, index.outind,20);
       check_results(res);
       
    }
}
