use std::{env, io, path::Path, process::exit};
use io::Write;
mod sh;
mod parse;

fn chdir(cmd: &Vec<&str>) -> Result<(),io::Error> {

    if cmd.len() > 1 {
        let path = Path::new(cmd[1]);
        env::set_current_dir(path)?; 
   } else {
       let home_dir = env::var_os("HOME").ok_or(io::Error::last_os_error())?;
       env::set_current_dir(home_dir.to_str().unwrap())?;
   }
    
    Ok(())
}

fn prepare(cmd: &String) {
    let mut vec_str = Vec::new();
    let sp: Vec<&str> = cmd.split_whitespace().collect();
    if sp[0].eq_ignore_ascii_case("cd"){
        match chdir(&sp) {
            Err(e) => println!("{}",e),
            Ok(_) => ()
        };
        return;
    }
    for x in sp.iter() {
        vec_str.push(x.to_string());
    }
    let mut op_index = parse::OpIndex::new(vec_str);
    op_index.set_index();
    match op_index.set_path() {
        true => sh::exec_cmd(&op_index),
        false => println!("Input or output file not provided! or pipe error")
    } 
     
}

fn main() {
    let mut command = String::new();
    let stdin = io::stdin();
    let mut counter = 1;
    loop {
        print!("mysh({}) ",counter);
        io::stdout().flush().unwrap();
        stdin.read_line(&mut command).unwrap();
        command.pop().unwrap();
        if command.eq_ignore_ascii_case("exit") {
            exit(0);
        }
        if command.len() != 0 {
            prepare(&command); 
        }
        command.clear();
        counter += 1;
    }
}
